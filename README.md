# README #

## Android Application ##

Android Application using [Parse.com](https://www.parse.com/) for data storage and [Picasso](http://square.github.io/picasso/) for upload image file.

This application uploads a picture on a database from parse.com

And you can get data from parse.com

### How do I get set up? ###

* Run the ParseStarterProject-debug.apk in the \build\outputs\apk folder on an Android Device
* Or run the project in [Android Studio](https://developer.android.com/sdk/index.html) (SDK 22)


To switch the database project edit the initialization line in StarterApplication.java
Like this :

``` Java
Parse.initialize(this, "key1", "key2");
```

#TODO#
Get push notification from server to get user profile data when the facial recognition is done.


## GUI de connexion à Parse avec le SDK PHP (Slim) ##


### Nos choix d'outils ###
* **Parse** pour la reception et l'envoi de données
* **Slim Framework** Pour la création de l'api rest

### Documentation ###
[Parse](https://www.parse.com/) est une bibliothèque disponnible en plusieurs languages qui permet de faire facilement le lien entre plusieurs plateformes et une même base de donnée. 

[Slim](http://www.slimframework.com/) est un framework PHP pour développer des applications et des APIs.

### Installation ###
Vous avez besoin de :

-un serveur apache.

-une connexion internet.

-activer le module mod_rewrite d'apache (clique gauche sur l'icon wamp/apache/modules/mod_rewrite).

-télécharger le dépot dans téléchargement sur bitbucket.

-dézipper et déplacer le dossier dans www pour wamp par exemple.


### les Urls : ###

[localhost/dossier/parse/](localhost) pour avoir le formulaire d'upload d'image.

[localhost/dossier/parse/view](localhost) pour voir les enregistrements des utilisateurs sur parse.

[localhost/dossier/parse/images](localhost) pour voir les enregistrements d'images uploadés sur parse.

### A Savoir ###
La bibliothèque de Parse utilisée est celle pour PHP.
Le language est donc du php (version 5.5).

### Contribution ###
Julien Metral
Damien Dabernat