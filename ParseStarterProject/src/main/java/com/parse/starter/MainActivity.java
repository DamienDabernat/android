package com.parse.starter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;


/* J'ai volontairement fait un truc simple
L'image upload� par la cam�ra n'est qu'une miniature en raison des
limitation d'Android. Sinon il fallait cr�er un fichier temp avant
de save l'image dessus, c'est chiant et long.
En revanche la gallerie renvoi la photo plein format.
 */

public class MainActivity extends Activity {

    private static final String LOG_TAG = "MainActivity";

    private static final int CAMERA_REQUEST = 60;
    private static final int GALLERY_PICTURE = 70;

    ImageView ivProfil;
    ImageView ivProfil2;
    EditText etObjectId;
    Bitmap bitmap;
    Button btGetData;
    TextView tvProfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        //Exemple pour faire une requete sur une table
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Utilisateur");
        // query.whereEqualTo("Username", "UnPrenom");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userList, ParseException e) {
                if (e == null) {
                    Log.d(LOG_TAG, "Retrieved " + userList.size() + " users");
                    for(ParseObject user : userList) {
                        Log.d(LOG_TAG, user.get("nom").toString());
                    }
                } else {
                    Log.d(LOG_TAG, "Error: " + e.getMessage());
                }
            }
        });


        //Recup des �l�ment du layout
        ivProfil = (ImageView) findViewById(R.id.ivProfil); //Photo prise
        ivProfil2 = (ImageView) findViewById(R.id.ivProfil2); //Photo re�u
        etObjectId = (EditText) findViewById(R.id.etObjectId); //Id de l'objet a recevoir
        btGetData = (Button) findViewById(R.id.btGetData); //Bouton de d�clanchement
        tvProfil = (TextView) findViewById(R.id.tvProfil); //Affichage des resultat

        btGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Utilisateur");
                query.getInBackground(etObjectId.getText().toString(), new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            String info = object.get("nom") + " " + object.get("prenom");
                            tvProfil.setText(info);
                            Log.d(LOG_TAG, object.get("url").toString());
                            Picasso.with(MainActivity.this).load(object.get("url").toString()).into(ivProfil2);
                        } else {
                            // Erreur ?!
                        }
                    }
                });
            }
        });

        Button btUpload = (Button) findViewById(R.id.btUpload);

        //Affichage et et lancement des fenetre de la gallerie ou de la l'apn
        btUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(MainActivity.this);
                myAlertDialog.setTitle("Telecharger une image");
                myAlertDialog.setMessage("Quelle application voulez-vous utilise ?");

                myAlertDialog.setPositiveButton("Galerie",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, GALLERY_PICTURE);
                            }
                        });

                myAlertDialog.setNegativeButton("Appareil photo",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                              Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                              startActivityForResult(takePicture, CAMERA_REQUEST);
                            }
                        });
                myAlertDialog.show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.bitmap != null) {
            ivProfil.setImageBitmap(this.bitmap);
        }
    }

    //On r�cup�re les photos ou l'image de la gallerie re�u
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case GALLERY_PICTURE:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    try {
                        UploadToServer(MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage));
                        //On affiche l'image
                        this.bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        ivProfil.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case CAMERA_REQUEST:
                if(resultCode == RESULT_OK){
                    Bundle b = data.getExtras();
                    Bitmap bmp=(Bitmap)b.get("data");

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    if (bmp != null) {
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    }
                    String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), bmp, "Title", null);
                    Uri selectedImage = Uri.parse(path);
                    try {
                        //Attention dans ce cas l'image sera une miniature
                        UploadToServer(MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage));
                        //On affiche l'image
                        this.bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        ivProfil.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
                break;
            case RESULT_CANCELED:
                Toast.makeText(getApplicationContext(), "Annule",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void UploadToServer(Bitmap bitmap){
        // Convert it to byte
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Compress image to lower quality scale 1 - 100
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] image = stream.toByteArray();

        // Create the ParseFile
        ParseFile file = new ParseFile("profil.jpg", image);
        // Upload the image into Parse Cloud
        file.saveInBackground();

        // Create a New Class called "ImageUpload" in Parse
        ParseObject imgupload = new ParseObject("ImageUpload");

        // Create a column named "ImageName" and set the string
        imgupload.put("ImageName", "profil");

        // Create a column named "ImageFile" and insert the image
        imgupload.put("ImageFile", file);

        // Create the class and the columns
        imgupload.saveInBackground();

        //En th�orie on pourrais recevoir l'id de l'objet retrouv� par le serveur
        //Sauf que bon, la c'est chiant. Du coup la je set manuellement un id
        //Comme �a si on appuis sur le bouton on retrouve l'objet associ� � 'id
        etObjectId.setText("DlcoBL9qYL");

        // Show a simple toast message
        Toast.makeText(MainActivity.this, "Image telecharge",
                Toast.LENGTH_SHORT).show();
    }
}
